const { expressReceiver } = require("./src/app")

module.exports.handler = serverlessExpress({
  app: expressReceiver.app,
})
