# Vendor Bot

計算零食補貼怎麼買最划算

## Slash Commands

#### `/vendor <amount>-<...prices><<min_remainder>`

計算 `<amount>` 可以買多少符合標價`<...prices>`的零食, 例如：

```
/vendor 200-22,33
```

輸出 Slack 訊息

> **Amount:** `200`<br/>**Remainder:** `2`<br/>**Snack Sets:**<br/>
>
> - `🥨22`×9
> - `🥨22`×6, `🍫33`×2
> - `🥨22`×3, `🍫33`×4
> - `🍫33`×5

實際情況為：

> `200 NTD` 零食補貼可以在剩下 `2 NTD` 的狀況下購買:
>
> - 9 包義美小泡芙
> - 6 包義美小泡芙 + 2 包健達繽紛樂
> - 3 包義美小泡芙 + 4 包健達繽紛樂
> - 5 包健達繽紛樂

可以透過 `/vendor help` 取得說明。

## Config

### App Config

#### OAuth Scope

需要的權限

- `chat:write`
- `chat:write.public`
- `chat:write.customize`
- `commands`

#### Command Slash

- command `/vendor`

### How to deploy on AWS Lambda

#### Step 1 | Setup `.env`

```
SLACK_BOT_TOKEN=<slack_app_oauth_token>
SLACK_SIGNING_SECRET=<slack_app_signing_secret>
AWS_ACCESS_KEY_ID=<aws_access_key>
AWS_SECRET_ACCESS_KEY=<aws_secret_access_key>
```

#### Step 2 | run command `npm run deploy`

```shell
npm run deploy
```

### How to dev

#### Step 1

```shell
> npm run start
> npm run ngrok
```

#### Step 2

Copy the https url generate from **ngrok**, and paste as app's **request url**. The request url end with sub path `slack/events`.

```
https://ngrok.url/slack/events
```
