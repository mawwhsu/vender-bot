const app = require('../src/app').default

async function start() {
  app.start(process.env.PORT || 3000);
  console.log('⚡️ Bolt app is running!');
}

start();