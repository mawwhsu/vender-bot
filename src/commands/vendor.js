const calculate = require("../utils/calculate").default

const COMMAND_VENDOR = "/vendor"

const COMMAND_VENDOR_REGEX = [
  /^([0-9 ]*)-([0-9\, ]*)<([0-9]*)$/,
  /^([0-9 ]*)-([0-9\, ]*)$/,
]

const SUB_COMMAND_HELP = "help"

const EMOJI_ICON = ":robot_face:"

// MARK: Datas

/**
 * @param {import('../calculate').CalculateResult} data
 * @returns {{
 *   remainder: number,
 *   solutions: import('../calculate').CheckoutData[]
 * }}
 */
function filterStealSolutions(data) {
  const remainder = Math.min(...data.remainders)
  const solutions = data.solutions
    .filter((d) => d.remainder === remainder)
    .sort((a, b) => new Set(b.carts).size - new Set(a.carts).size)
  return {
    solutions,
    remainder,
  }
}

/**
 * @param {number} carts
 * @returns {Record<number, number>}
 */
function convertCartListToMap(carts) {
  const result = {}
  let i = 0
  while (i < carts.length) {
    const amount = carts[i++]
    result[amount] = (result[amount] || 0) + 1
  }

  return result
}

// MARK: Blocks

const SEED_INDEX = new Date().getDate()

const SNACK_LIST = [
  "🥨",
  "🍭",
  "🥐",
  "🍬",
  "🧀",
  "🧃",
  "🧇",
  "🥝",
  "🍟",
  "🍧",
  "🍕",
  "🥫",
  "🌭",
  "🥯",
  "🍪",
  "🥤",
  "🍺",
  "🍫",
  "🍮",
  "🍦",
  "🍨",
  "🧁",
  "🍰",
  "🍡",
  "🍢",
  "🥮",
  "🍘",
]

const DIGIT_ICON = [
  "0️⃣",
  "1️⃣",
  "2️⃣",
  "3️⃣",
  "4️⃣",
  "5️⃣",
  "6️⃣",
  "7️⃣",
  "8️⃣",
  "9️⃣",
  "🔟",
]

/** @type {import('@slack/bolt').RespondArguments} */
const EXAMPLE_MESSAGE = {
  response_type: "ephemeral",
  icon_emoji: EMOJI_ICON,
  blocks: [
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text: "Command `/vendor 5-3,2,1` gets:",
      },
    },
    generateResultBlock({
      amount: 5,
      remainder: 0,
      solutions: [
        { carts: [1], remainder: 0 },
        { carts: [1, 1, 1, 2], remainder: 0 },
        { carts: [1, 1, 3], remainder: 0 },
        { carts: [1, 2, 2], remainder: 0 },
        { carts: [2, 3], remainder: 0 },
      ],
    }),
    {
      type: "section",
      text: {
        type: "mrkdwn",
        text:
          "The command including all snack is `/vendor 200-19,21,22,24,29,30,33,36,37,38,39,37,43,50,56`.",
      },
    },
  ],
}

/**
 * @param {number} index
 * @returns {string}
 */
function snackIcon(index) {
  return SNACK_LIST[(index + SEED_INDEX) % SNACK_LIST.length]
}

/**
 * @param {number} index
 * @returns {string}
 */
function digitIcon(value) {
  const tens = new Array(Math.floor(value / 10)).fill(DIGIT_ICON[10]).join("")
  let digit = value % 10
  digit = digit ? DIGIT_ICON[digit] : ""
  return tens + digit
}

/**
 * @param {[amount: number, count: number]} tuple
 * @returns {string}
 */
function formatProduct(tuple) {
  const [amount, count] = tuple
  return `\`${snackIcon(amount)}${amount}\`×${count}`
}

/**
 * @param {import('../calculate').CheckoutData} solution
 * @param {string=} prefix
 * @returns {string}
 */
function formateSolution(prefix, solution) {
  const { carts } = solution
  let message = `${Object.entries(convertCartListToMap(carts))
    .map(formatProduct)
    .join(", ")}`
  if (prefix) {
    message = prefix + message
  }

  return message
}

/**
 * @param {{
 *   amount: number
 *   remainder: number
 *   solutions: import('../calculate').CheckoutData
 * }} data
 * @returns {import('@slack/bolt').KnownBlock}
 */
function generateResultBlock(data) {
  const { amount, remainder, solutions } = data
  return {
    type: "section",
    text: {
      type: "mrkdwn",
      text:
        `*Amount:* \`${amount}\`\n` +
        `*Remainder:* \`${remainder}\`\n` +
        "*Snack Sets:*\n" +
        solutions.map((solution) => formateSolution("• ", solution)).join("\n"),
    },
  }
}

/**
 * @param {string} text
 * @returns {import('@slack/bolt').RespondArguments}
 */
function generateErrorMessage(text) {
  return {
    response_type: "ephemeral",
    icon_emoji: EMOJI_ICON,
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text:
            `Command \`${COMMAND_VENDOR} ${text}\` is wrong.\n` +
            "Please type `/vendor help` for command example.",
        },
      },
    ],
  }
}

/**
 * @param {string} userName
 * @returns {import('@slack/bolt').RespondArguments}
 */
function generatePoorMessage(userName) {
  return {
    icon_emoji: EMOJI_ICON,
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `Oops @${userName},\nYou are too poor to buy snacks...`,
        },
      },
    ],
  }
}

/**
 * @param {{
 *   userName: string
 *   amount: number
 *   remainder: number
 *   solutions: import('../calculate').CheckoutData
 * }} data
 * @returns {import('@slack/bolt').RespondArguments}
 */
function generateMessage(data) {
  const { userName } = data
  return {
    icon_emoji: EMOJI_ICON,
    blocks: [
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: `Hi @${userName},\n` + "Here is the best deals...",
        },
      },
      generateResultBlock(data),
    ],
  }
}

// MARK: Main

/**
 * @param {import('@slack/bolt').AllMiddlewareArgs & import('@slack/bolt').SlackCommandMiddlewareArgs} args
 */
async function listener(args) {
  const { command, ack, say, respond, client } = args
  await ack()

  const { user_name: userName } = command
  let { text } = command
  text = text.trim()

  let valid = null
  let index = 0
  while (!valid && index < COMMAND_VENDOR_REGEX.length) {
    const regex = COMMAND_VENDOR_REGEX[index++]
    valid = regex.exec(text)
  }
  if (!valid) {
    if (text !== SUB_COMMAND_HELP) {
      respond(generateErrorMessage(text))
      return
    }

    respond(EXAMPLE_MESSAGE)
    return
  }

  // SECTION

  const [, rawAmount, rawCandidates, rawMinRemainder] = valid
  const amount = parseInt(rawAmount.trim(), 10)
  const candidates = rawCandidates
    .trim()
    .split(/[, -]/)
    .filter((v) => !!v)
    .map((v) => parseInt(v.trim(), 10))
  const minRemainder = rawMinRemainder && parseInt(rawMinRemainder.trim(), 10)

  let calculated
  try {
    calculated = filterStealSolutions(
      calculate(amount, candidates, minRemainder)
    )
  } catch {
    respond(generateErrorMessage(text))
    return
  }

  const { solutions, remainder } = calculated
  if (!solutions.length) {
    const message = generatePoorMessage(userName)
    say(message).catch((e) => {
      console.error(e)
      respond({
        ...message,
        response_type: "ephemeral",
      })
    })
    return
  }

  // SECTION

  const mainMessage = generateMessage({
    userName: userName,
    amount,
    remainder,
    solutions: solutions.splice(0, 5),
  })
  const { ts, channel } = await say(mainMessage).catch((e) => {
    console.error(e)
    return respond({
      ...mainMessage,
      response_type: "ephemeral",
    })
  })

  let count = 0
  while (solutions.length && count++ < 9) {
    const message = {
      icon_emoji: EMOJI_ICON,
      blocks: [
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: solutions
              .splice(0, Math.min(solutions.length, 5))
              .map((solution) => formateSolution("• ", solution))
              .join("\n"),
          },
        },
      ],
    }
    client.chat
      .postMessage({
        ...message,
        channel,
        thread_ts: ts,
      })
      .catch((e) => {
        console.error(e)
        return respond({
          ...message,
          response_type: "ephemeral",
        })
      })
  }
}

module.exports = {
  command: COMMAND_VENDOR,
  listener,
}
