// MARK: Checkout

/**
 * @typedef {object} CheckoutData
 * @property {number} remainder
 * @property {number[]} carts sorted array
 */

/**
 * @param {string} val
 * @returns {number}
 */
function parseStringToNumber(val) {
  return parseInt(val, 10)
}

/**
 * @param {CheckoutData} checkout
 * @returns {string}
 */
function encodeCheckout(checkout) {
  const { remainder, carts } = checkout
  return remainder + "_" + carts.join(",")
}

/**
 * @param {string} encoded
 * @returns {CheckoutData}
 */
function decodeCheckout(encoded) {
  const arr = encoded.split("_")
  const remainder = parseStringToNumber(arr[0])
  const carts = arr[1].split(",").map(parseStringToNumber)
  return {
    remainder,
    carts,
  }
}

// MARK: Queue

/**
 * @typedef {object} QueueData
 * @property {number} amount
 * @property {number[]} carts sorted array
 * @property {number[]} candidates
 */

/**
 * @typedef {object} QueueConfig
 * @property {number} minRemainder
 * @property {Record<number, number[]>} cache
 */

/**
 * @typedef {object} QueueAction
 * @property {'queue'} type
 * @property {QueueData} data
 */

/**
 * @typedef {object} CheckoutAction
 * @property {'chekcout'} type
 * @property {CheckoutData} data
 */

/**
 * @param {QueueData} data
 * @returns {string}
 */
function encodeQueue(data) {
  const { amount, carts } = data
  return amount + carts.toString()
}

// MARK: Main

/**
 * @typedef {object} CalculateResult
 * @property {Checkout[]} solutions all possible snack sets
 * @property {number[]} remainders all possible remainders
 */

/**
 * @param {QueueConfig} config
 * @param {QueueData} data
 * @returns {Generator<QueueAction | CheckoutAction, void, void>}
 */
function* exec(config, data) {
  const { minRemainder, cache } = config
  const { amount, carts, candidates } = data
  let i = 0
  while (i < candidates.length) {
    const value = candidates[i++]
    const delta = amount - value
    if (delta < 0) {
      continue
    }

    let nextCandidates = cache[delta]
    if (!nextCandidates) {
      nextCandidates = []
      cache[delta] = nextCandidates
      let j = 0
      while (j < candidates.length) {
        const v = candidates[j++]
        if (delta >= v) {
          nextCandidates.push(v)
        }
      }
    }

    const arr = [...carts, value].sort()
    if (!nextCandidates.length) {
      if (delta > minRemainder) {
        continue
      }

      /** @type {CheckoutAction} */
      const checkoutAction = {
        type: "checkout",
        data: {
          remainder: delta,
          carts: arr,
        },
      }
      yield checkoutAction
      continue
    }

    /** @type {QueueAction} */
    const queueAction = {
      type: "queue",
      data: {
        amount: delta,
        carts: arr,
        candidates: nextCandidates,
      },
    }
    yield queueAction
  }
}

/**
 * @param {number} amount
 * @param {number[]} candidates
 * @param {number} minRemainder
 * @returns {CalculateResult}
 */
function calculate(amount, candidates, minRemainder = 10) {
  if (candidates.some((c) => c <= 0)) {
    throw Error("children of `candidates` must be greater then `0`")
  }

  if (Number.isNaN(minRemainder) || typeof minRemainder !== "number") {
    minRemainder = 10
  }

  /** @type {QueueData} */
  const seedData = {
    amount: amount,
    carts: [],
    candidates,
  }
  const seedKey = encodeQueue(seedData)

  const checkoutSet = new Set()
  const remainderSet = new Set()
  const queue = [seedData]
  const queueRegisterSet = new Set(seedKey)

  /** @type {QueueConfig} */
  const config = {
    minRemainder,
    cache: {},
  }
  while (queue.length) {
    for (const action of exec(config, queue.pop())) {
      const { type, data } = action
      switch (type) {
        case "queue":
          const key = encodeQueue(data)
          if (!queueRegisterSet.has(key)) {
            queue.push(data)
            queueRegisterSet.add(key)
          }
          break
        case "checkout":
          checkoutSet.add(encodeCheckout(data))
          remainderSet.add(data.remainder)
          break
      }
    }
  }

  /** @type {CalculateResult} */
  const result = {
    solutions: Array.from(checkoutSet).map(decodeCheckout),
    remainders: Array.from(remainderSet),
  }
  return result
}

// MAIN: Exports

module.exports = {
  default: calculate,
}
