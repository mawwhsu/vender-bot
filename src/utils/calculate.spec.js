const calculate = require("./calculate").default

/**
 * @param {import('./calculate').CalculateResult} data
 * @returns {object}
 */
function toString(data) {
  return {
    solutions: data.solutions
      .map((checkout) => {
        const { carts, remainder } = checkout
        return carts.sort().toString() + remainder
      })
      .sort(),
    remainders: data.remainders.sort(),
  }
}

test("calculate", () => {
  /** @type {import('./calculate').CalculateResult} */
  const expected = {
    solutions: [
      {
        carts: [4, 3],
        remainder: 0,
      },
      {
        carts: [4, 2],
        remainder: 1,
      },
      {
        carts: [3, 3],
        remainder: 1,
      },
      {
        carts: [3, 2, 2],
        remainder: 0,
      },
      {
        carts: [2, 2, 2],
        remainder: 1,
      },
    ],
    remainders: [0, 1],
  }

  const received = calculate(7, [4, 3, 2])
  expect(toString(received)).toEqual(toString(expected))
})
