const { App, ExpressReceiver } = require("@slack/bolt")
const vender = require("./commands/vendor")

const expressReceiver = new ExpressReceiver({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  processBeforeResponse: true,
})

const app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  receiver: expressReceiver,
})

app.command(vender.command, vender.listener)

module.exports = {
  default: app,
  expressReceiver,
}
